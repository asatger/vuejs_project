# VueJS student project

Aurélien Satger

aurelien.satger@gmail.com

## How to install ?

### Clone 
HTTPS:
```console
https://gitlab.com/asatger/vuejs_project.git
```
SSH:
```console
git@gitlab.com:asatger/vuejs_project.git
```
### Setup
Install dependencies:
```console
npm install
```
Compile webpack:
```console
node_modules/.bin/webpack --progress --hide-modules
```

### Run
Run server: 
```console
node_modules/.bin/webpack-dev-server --progress --open --hot --port 8080
```
Then go to http://localhost:8080/

## Enjoy ! :)