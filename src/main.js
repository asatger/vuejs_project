import Vue from 'vue' //librairie "vue" dans node_modules
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import app from './app.vue' //fichier app.vue local

import MovieItemComponent from './components/movieitem.vue'
import HomeComponent from './components/home.vue'
import MovieEditComponent from './components/movieedit.vue'
import MovieAddComponent from './components/movieadd.vue'
import HeaderComponent from './components/_header.vue'
import FooterComponent from './components/_footer.vue'
import FormComponent from './components/_form.vue'
import MovieDetailsComponent from './components/moviedetails.vue'

import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader

// import movies data
import movies_json from './static/movies.json'

Vue.use(VueRouter);
Vue.use(Vuetify);

// create components
Vue.component('header-item', HeaderComponent);
Vue.component('footer-item', FooterComponent);
Vue.component('home', HomeComponent);
Vue.component('movie-item', MovieItemComponent);
Vue.component('movie-edit', MovieEditComponent);
Vue.component('movie-add', MovieAddComponent);
Vue.component('movie-form', FormComponent);

// set the shared variable movies accessible anywhere in the app
window.shared_data = {
  movies: movies_json.movies
}

// initialize routes
const routes = [
  { path: "/", name: "home", component: HomeComponent },
  { path: "/movie/:id/edit", name: "edit", component: MovieEditComponent },
  { path: "/add ", name: "add", component: MovieAddComponent },
  { path: "/movie/:id ", name: "movie-details", component: MovieDetailsComponent }
];

// create rooter
const router = new VueRouter({
  routes
});

new Vue({
  el: '#app',
  render: h => h(app),
  router
})
